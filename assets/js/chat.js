(function (socket, $) {

	if (window.location.pathname == '/chat') {

		$('.chatSend').click(function (e) {
			sendMessage(this, e);
		});

		$('.chatInput').keypress(function (e) {
			if (e.keyCode == 13) {
				sendMessage(this, e);
			}
		});

		$('#newRoom').click(function (e) {
			e.preventDefault();
			var index = $('#chatRooms li').length-1;
			$('#chatRooms li:last').before('<li><a href="#room'+index+'" data-toggle="tab">Room '+index+'</a></li>');
			$('#chatRoomsContent').append('<div class="tab-pane fade" id="room'+index+'"></div>')
			$('#chatRooms a[href=#room'+index+']').tab('show');
		});

		function sendMessage (me, e) {
			e.preventDefault();
			var form = $(me).parents('form');
			var message = form.find('input').val();
			var username = $('h5.username').text();
			socket.post('/message', { username: username, body: message });
			form.find('input').val('');
		}

		socket.get('/message', function (messages) {
		  messages.forEach(function (message) {
		    $('#main.tab-pane').append('<p><strong>'+(message.username?message.username:'Anonymous')+'</strong>: '+message.body+'</p>');
		    $('#main.tab-pane').animate({
		      scrollTop: $('#main.tab-pane')[0].scrollHeight
		    }, 200);
		  });
		});

	}


})( window.socket, jQuery );