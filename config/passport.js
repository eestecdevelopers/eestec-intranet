// Location: /config/passport.js
var passport    = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    TwitterStrategy  = require('passport-twitter').Strategy,
    bcrypt = require('bcrypt');

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});

passport.use('local-login', new LocalStrategy({
    passReqToCallback: true
  },
  function(req, username, password, done) {
    process.nextTick(function(){
      User.findOne({'username': username}).done(function(err, user) {
        if (err) { return done(err); }
        if (!user || user.length < 1) { return done(null, false, req.flash('loginMessage', 'No user found')); }
        if (!user.validPassword(password)) { return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); }
        return done(null, user);
      });
    });
  })
);

passport.use('local-register', new LocalStrategy({
    passReqToCallback: true
  },
  function(req, username, password, done) {
    process.nextTick(function(){
      User.findOne({'username': username}, function(err, existingUser){
        if (err) { return done(err); }
        if (existingUser) { return done(null, false, req.flash('registerMessage', 'That username is already taken.')); }
        if (req.user) {
          var user = req.user[0];
          user.username = username;
          user.password = generateHash(password);
          user.save(function(err){
            if (err) { throw err; }
            return done(null, user);
          });
        } else {
          if (req.body.password !== req.body.confirmPassword) { return done(null, false, req.flash('registerMessage', 'Passwords do not match.')); }
          User.create({
            username: username,
            password: generateHash(password),
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email
          }).done(function(err, user){
            if (err) { throw err; }
            return done(null, user);
          });
        }
      });
    });
  })
);

passport.use('google', new GoogleStrategy({
    clientID: '143367867444.apps.googleusercontent.com',
    clientSecret: 'PV27EUzgul0m4OXv2jWGuol3',
    callbackURL: 'http://localhost:1337/auth/google/callback',
    passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },
  function(req, token, refreshToken, profile, done) {
    process.nextTick(function() {
      if (!req.user) {
        User.findOne({ 'googleId' : profile.id }, function(err, user) {
          if (err)
            return done(err);
          if (user) {
            if (!user.googleToken) {
              user.googleToken = token;
              user.googleName  = profile.displayName;
              user.googleEmail = profile.emails[0].value; // pull the first email
              user.save(function(err) {
                if (err)
                  throw err;
                return done(null, user);
              });
            }
            return done(null, user);
          } else {
            User.create({
              googleId: profile.id,
              googleToken: token,
              googleName: profile.displayName,
              googleEmail: profile.emails[0].value
            }).done(function(err, user){
              if (err) { throw err; }
              return done(null, user);
            });
          }
        });
      } else {
        var user         = req.user[0]; // pull the user out of the session
        user.googleId    = profile.id;
        user.googleToken = token;
        user.googleName  = profile.displayName;
        user.googleEmail = profile.emails[0].value; // pull the first email
        user.save(function(err) {
          if (err)
            throw err;
          return done(null, user);
        });
      }
    });
  }
));

passport.use('facebook', new FacebookStrategy({
    clientID: '221799937871399',
    clientSecret: '5b090a1b68047eb27d4a196426a3e2b1',
    callbackURL: 'http://localhost:1337/auth/facebook/callback',
    passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },
  function(req, token, refreshToken, profile, done) {
    process.nextTick(function() {
      if (!req.user) {
        User.findOne({ 'facebookId' : profile.id }, function(err, user) {
          if (err)
            return done(err);
          if (user) {
            if (!user.facebookToken) {
              user.facebookToken = token;
              user.facebookName  = profile.name.givenName + ' ' + profile.name.familyName;
              user.facebookEmail = profile.emails[0].value; // pull the first email
              user.save(function(err) {
                if (err)
                  throw err;
                return done(null, user);
              });
            }
            return done(null, user);
          } else {
            User.create({
              facebookId: profile.id,
              facebookToken: token,
              facebookName: profile.name.givenName + ' ' + profile.name.familyName,
              facebookEmail: profile.emails[0].value
            }).done(function(err, user){
              if (err) { throw err; }
              return done(null, user);
            });
          }
        });
      } else {
        var user           = req.user[0]; // pull the user out of the session
        user.facebookId    = profile.id;
        user.facebookToken = token;
        user.facebookName  = profile.name.givenName + ' ' + profile.name.familyName;
        user.facebookEmail = profile.emails[0].value; // pull the first email
        user.save(function(err) {
          if (err)
            throw err;
          return done(null, user);
        });
      }
    });
  }
));

passport.use('twitter', new TwitterStrategy({
    consumerKey: 'Ysf6pepGBXzvBvNX53Csn6LLu',
    consumerSecret: 'b2cZKE6an5SaL4syLnf772fiFKHc1ahiyaU7INEvSoPmWmO6O5',
    callbackURL: 'http://localhost:1337/auth/twitter/callback',
    passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },
  function(req, token, refreshToken, profile, done) {
    console.log(profile);
    process.nextTick(function() {
      if (!req.user) {
        User.findOne({ 'twitterId' : profile.id }, function(err, user) {
          if (err)
            return done(err);
          if (user) {
            if (!user.twitterId) {
              user.twitterToken = token;
              user.twitterUsername = profile.username;
              user.twitterName  = profile.displayName;
              user.save(function(err) {
                if (err)
                  throw err;
                return done(null, user);
              });
            }
            return done(null, user);
          } else {
            User.create({
              twitterId: profile.id,
              twitterToken: token,
              twitterName: profile.displayName,
              twitterUsername: profile.username
            }).done(function(err, user){
              if (err) { throw err; }
              return done(null, user);
            });
          }
        });
      } else {
        var user             = req.user[0]; // pull the user out of the session
        user.twitterId       = profile.id;
        user.twitterToken    = token;
        user.twitterName     = profile.displayName;
        user.twitterUsername = profile.username;
        user.save(function(err) {
          if (err)
            throw err;
          return done(null, user);
        });
      }
    });
  }
));

module.exports = {
 express: {
    customMiddleware: function(app){
      console.log('express midleware for passport');
      app.use(passport.initialize());
      app.use(passport.session());
    }
  }
};