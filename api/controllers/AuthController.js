/**
 * AuthController
 *
 * @module      :: Controller
 * @description :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var passport = require('passport');

module.exports = {

    login: function(req,res){
      if (req.user) { res.redirect('/profile'); }
      res.view('auth/login', { user: req.user?req.user[0]:null, message: req.flash('loginMessage') });
    },

    processLogin: function(req,res,next){
      passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
      }, function(err, user, info) {
        if (err) { next(err); }
        if (!user) { return res.redirect('/login'); }
        req.login(user, function(err) {
          if (err) { return next(err); }
          User.update(user.id, { online: true }, function (err) {
            if (err) return next(err);
            User.publishUpdate(user.id, { online: true });
          });
          return res.redirect('/');
        });
      })(req,res,next);
    },

    register: function(req,res){
      res.view('auth/register', { user: req.user?req.user[0]:null, message: req.flash('registerMessage') });
    },

    registerProcess: function(req,res,next){
      passport.authenticate('local-register', {
        successRedirect: '/profile',
        failureRedirect: '/register',
        failureFlash: true
      }, function(err, user, info) {
        if (err) { next(err); }
        if (!user) { return res.view('auth/register', { user: req.user?req.user[0]:null, message: req.flash('registerMessage') }); }
        req.login(user, function(err) {
          if (err) { return next(err); }
          return res.redirect('/');
        });
      })(req,res,next);
    },

    logout: function(req,res){
      User.update(req.user[0].id, { online: false }, function (err) {
        if (err) return next(err);
        User.publishUpdate(req.user[0].id, { online: false });
      });
      req.logout();
      res.redirect('/');
    },

    connectLocal: function(req,res){
      res.view('auth/connect-local', { message: req.flash('loginMessage') });
    },

    processConnectLocal: function(req,res){
      passport.authenticate('local-register', {
        successRedirect : '/profile',
        failureRedirect : '/connect/local',
        failureFlash : true
      });
    },

    googleLogin: function(req,res,next){
      passport.authenticate('google', { scope : ['profile', 'email'] }, function(err){
        if (err) { next(err); }
        next();
      })(req,res,next);
    },

    googleCallback: function(req,res,next){
      passport.authenticate('google', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }, function(err, user, info){
        if (err) { next(err); }
        if (!user) { return res.redirect('/login'); }
        req.login(user, function(err) {
          if (err) { return next(err); }
          return res.redirect('/');
        });
      })(req,res,next);
    },

    googleConnect: function(req,res,next){
      passport.authenticate('google', { scope : ['profile', 'email'] }, function(err){
        if (err) { next(err); }
        next();
      })(req,res,next);
    },

    googleConnectCallback: function(req,res,next){
      passport.authorize('google', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }, function(err){
        if (err) { next(err); }
        return res.redirect('/profile');
      })(req,res,next);
    },

    facebookLogin: function(req,res,next){
      passport.authenticate('facebook', { scope : 'email' }, function(err){
        if (err) { next(err); }
        next();
      })(req,res,next);
    },

    facebookCallback: function(req,res,next){
      passport.authenticate('facebook', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }, function(err, user, info){
        if (err) { next(err); }
        if (!user) { return res.redirect('/login'); }
        req.login(user, function(err) {
          if (err) { return next(err); }
          return res.redirect('/');
        });
      })(req,res,next);
    },

    facebookConnect: function(req,res,next){
      passport.authenticate('facebook', { scope : 'email' }, function(err){
        if (err) { next(err); }
        next();
      })(req,res,next);
    },

    facebookConnectCallback: function(req,res,next){
      passport.authorize('facebook', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }, function(err){
        if (err) { next(err); }
        return res.redirect('/profile');
      })(req,res,next);
    },

    twitterLogin: function(req,res,next){
      passport.authenticate('twitter', { scope : 'email' }, function(err){
        if (err) { next(err); }
        next();
      })(req,res,next);
    },

    twitterCallback: function(req,res,next){
      passport.authenticate('twitter', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }, function(err, user, info){
        if (err) { next(err); }
        if (!user) { return res.redirect('/login'); }
        req.login(user, function(err) {
          if (err) { return next(err); }
          return res.redirect('/');
        });
      })(req,res,next);
    },

    twitterConnect: function(req,res,next){
      passport.authenticate('twitter', { scope : 'email' }, function(err){
        if (err) { next(err); }
        next();
      })(req,res,next);
    },

    twitterConnectCallback: function(req,res,next){
      passport.authorize('twitter', {
        successRedirect : '/profile',
        failureRedirect : '/'
      }, function(err){
        if (err) { next(err); }
        return res.redirect('/profile');
      })(req,res,next);
    },

    unlinkLocal: function(req,res,next){
      var user = req.user[0];
      user.username = undefined;
      user.password = undefined;
      user.email = undefined;
      user.firstName = undefined;
      user.lastName = undefined;
      user.save(function(err) {
        if (err) { next(err); }
        res.redirect('/profile');
      });
    },

    unlinkGoogle: function(req,res,next){
      var user = req.user[0];
      user.googleToken = undefined;
      user.googleId = undefined;
      user.googleEmail = undefined;
      user.googleName = undefined;
      user.save(function(err) {
        if (err) { next(err); }
        res.redirect('/profile');
      });
    },

    unlinkFacebook: function(req,res,next){
      var user = req.user[0];
      user.facebookToken = undefined;
      user.facebookId = undefined;
      user.facebookEmail = undefined;
      user.facebookName = undefined;
      user.save(function(err) {
        if (err) { next(err); }
        res.redirect('/profile');
      });
    },

    unlinkTwitter: function(req,res,next){
      var user = req.user[0];
      user.twitterToken = undefined;
      user.twitterId = undefined;
      user.twitterUsername = undefined;
      user.twitterName = undefined;
      user.save(function(err) {
        if (err) { next(err); }
        res.redirect('/profile');
      });
    },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to AuthController)
   */
  _config: {}


};
