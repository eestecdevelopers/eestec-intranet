/**
 * User
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

var bcrypt = require('bcrypt');

module.exports = {

  // autoPK: false,

  attributes: {

    username: {
      type: 'string',
      unique: true
    },

    firstName: {
      type: 'string'
    },

    lastName: {
      type: 'string'
    },

    email: {
      type: 'email',
      unique: true
    },

    password: {
      type: 'string',
      minLength: 6
    },

    online: {
      type: 'boolean',
      defaultsTo: false
    },

  	googleId: 'string',
    googleToken: 'string',
    googleEmail: 'string',
    googleName: 'string',

    facebookId: 'string',
    facebookToken: 'string',
    facebookEmail: 'string',
    facebookName: 'string',

    twitterId: 'string',
    twitterToken: 'string',
    twitterUsername: 'string',
    twitterName: 'string',

    //Override toJSON method to remove password from API
    toJSON: function() {
      var obj = this.toObject();
      // Remove the password object value
      delete obj.password;
      // return the new object without password
      return obj;
    },

    validPassword: function(password) {
      return bcrypt.compareSync(password, this.password);
    }

  }

};

generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
